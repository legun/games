// let config = {
//     type: Phaser.CANVAS,
//     width: 960,
//     height: 640,
//     physics: {
//         default: 'arcade',
//         // arcade: {
//         //     gravity: {y: 500},
//         //     debug: false
//         // }
//     },
//     scene: {
//         preload: preload,
//         create: create,
//         update: update
//     }
// };

let game = new Phaser.Game(480, 320, Phaser.CANVAS, null, {
    preload: preload, create: create, update: update
});
let color;
let ball;
let ball2;
let paddle;
let bricks;
let newBrick;
let brickInfo;
let scoreText;
let score = 0;
let playing = false;
let startButton;
let colors = ['#B8860B', '#00FF00','#00FFFF','#FFFF00','#2F4F4F', '#98FB98','#E9967A', '#F0E68C', '#808000', '#A52A2A'];
let gameOver;

function preload() {
    game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    game.scale.pageAlignHorizontally = true;
    game.scale.pageAlignVertically = true;
    game.stage.backgroundColor = color;
    game.load.image('ball', 'img/ball.png');
    game.load.image('ballGreen', 'img/ballGreen.png');
    game.load.image('paddle', 'img/paddle.png');
    game.load.image('brick', 'img/brick.png');
    game.load.spritesheet('button', 'img/button.png', 120, 40);
    // game.load.spritesheet('ball', 'img/wobble.png', 20, 20);
}

function create() {
    game.physics.startSystem(Phaser.Physics.ARCADE);
    ball = game.add.sprite(game.world.width * 0.5, game.world.height - 25, 'ball');
    // ball = game.add.sprite(50, 250, 'ball');
    // ball.animations.add('wobble', [0,1,0,2,0,1,0,2,0], 24);
    ball.anchor.set(0.5);
    paddle = game.add.sprite(game.world.width * 0.5, game.world.height - 5, 'paddle');
    paddle.anchor.set(0.5, 1);
    game.physics.enable(ball, Phaser.Physics.ARCADE);
    game.physics.enable(paddle, Phaser.Physics.ARCADE);
    ball.body.collideWorldBounds = true;
    ball.body.bounce.set(1);
    // ball.body.velocity.set(150, -150);
    paddle.body.immovable = true;
    game.physics.arcade.checkCollision.down = false;
    ball.checkWorldBounds = true;
    ball.events.onOutOfBounds.add(function () {
        alert('Game over!');
        location.reload();
    }, this);
    initBricks();
    scoreText = game.add.text(5, 5, 'Points: 0', {font: '18px Arial', fill: '#0095DD'});
    startButton = game.add.button(game.world.width*0.5, game.world.height*0.5, 'button', startGame, this, 1, 0, 2);
    startButton.anchor.set(0.5);
}
function update() {
    if (gameOver){
        gameOver = false;
        setTimeout(function(){
            alert('You won the game, congratulations!');
            location.reload();
        }, 500);
    }
    game.physics.arcade.collide(ball, paddle, ballHitPaddle);
    game.physics.arcade.collide(ball, bricks, ballHitBrick);
    game.physics.arcade.collide(ball2, paddle, ballHitPaddle);
    game.physics.arcade.collide(ball2, bricks, ballHitBrick);
    if(playing) {
        paddle.x = game.input.x || game.world.width*0.5;
    }
}

function initBricks() {
    brickInfo = {
        width: 50,
        height: 20,
        count: {
            row: 3,
            col: 7
        },
        offset: {
            top: 50,
            left: 60
        },
        padding: 10
    };
    bricks = game.add.group();
    for (let c = 0; c < brickInfo.count.col; c++) {
        for (let r = 0; r < brickInfo.count.row; r++) {
            let brickX = (c * (brickInfo.width + brickInfo.padding)) + brickInfo.offset.left;
            let brickY = (r * (brickInfo.height + brickInfo.padding)) + brickInfo.offset.top;
            newBrick = game.add.sprite(brickX, brickY, 'brick');
            game.physics.enable(newBrick, Phaser.Physics.ARCADE);
            newBrick.body.immovable = true;
            newBrick.anchor.set(0.5);
            bricks.add(newBrick);
        }
    }
}

function ballHitBrick(ball, brick) {
    let killTween = game.add.tween(brick.scale);
    killTween.to({x:0,y:0}, 200, Phaser.Easing.Linear.None);
    killTween.onComplete.addOnce(function(){
        brick.kill();
    }, this);
    killTween.start();
    score += 10;
    scoreText.setText('Points: ' + score);

    let count_alive = 0;
    for (let i = 0; i < bricks.children.length; i++) {
        if (bricks.children[i].alive === true) {
            count_alive++;
        }
    }
    if (count_alive === 1) {
        gameOver = true;
    }
    game.stage.backgroundColor = changeColor();
    createBall();
}

function ballHitPaddle(ball, paddle) {
    ball.animations.play('wobble');
    ball.body.velocity.x = -1*5*(paddle.x-ball.x);
}
function startGame() {
    startButton.destroy();
    ball.body.velocity.set(150, -150);
    playing = true;
}
function changeColor(min = 0.5, max = colors.length - 1) {
    let rand = min - 0.5 + Math.random() * (max - min + 1);
    return colors[Math.round(rand)];
}
function createBall() {
    if (score === 40){
        ball2 = game.add.sprite(game.world.width * 0.5, game.world.height - 25, 'ballGreen');
        ball2.anchor.set(0.5);
        game.physics.enable(ball2, Phaser.Physics.ARCADE);
        ball2.body.collideWorldBounds = true;
        ball2.body.bounce.set(1);
        ball2.body.velocity.set(150, -150);
    }
}